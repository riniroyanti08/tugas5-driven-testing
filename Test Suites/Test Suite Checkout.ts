<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Checkout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>eb88fc0f-45af-404a-b50b-b3fb946af285</testSuiteGuid>
   <testCaseLink>
      <guid>aac1364f-e90f-4b2d-9237-6ea4c79a886a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TS02-Produk/TC004-Checkout</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f74f599f-48aa-4bfd-9e0c-40dc441ca34b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Data Checkout</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>f74f599f-48aa-4bfd-9e0c-40dc441ca34b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>FirstName</value>
         <variableId>b2a67490-57ff-40b5-a6ed-ace42196858f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f74f599f-48aa-4bfd-9e0c-40dc441ca34b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>LastName</value>
         <variableId>8d9fb6e7-b0d8-4199-b1d6-7638277689d1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f74f599f-48aa-4bfd-9e0c-40dc441ca34b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Zip</value>
         <variableId>af05c072-4990-47c0-acca-4667ca957764</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
