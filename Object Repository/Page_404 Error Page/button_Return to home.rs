<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Return to home</name>
   <tag></tag>
   <elementGuidId>d88bbce0-10fd-42cb-85ca-fb1a58a004f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.MuiButtonBase-root.MuiButton-root.MuiButton-contained.MuiButton-containedAccentGreen.MuiButton-sizeMedium.MuiButton-containedSizeMedium.MuiButton-disableElevation.MuiButton-fullWidth.MuiButton-root.MuiButton-contained.MuiButton-containedAccentGreen.MuiButton-sizeMedium.MuiButton-containedSizeMedium.MuiButton-disableElevation.MuiButton-fullWidth.css-as8hpy</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2cd8efa7-3554-4eeb-85f1-4a3c6db5ba59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedAccentGreen MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-disableElevation MuiButton-fullWidth MuiButton-root MuiButton-contained MuiButton-containedAccentGreen MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-disableElevation MuiButton-fullWidth css-as8hpy</value>
      <webElementGuid>7d90e408-7e73-4b4a-8c20-f609e88ee28d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>f191ac1c-7ee7-432e-9446-49c4ebc84acb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>18e203d9-1d91-4a36-8c6d-a7a082695611</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Return to home</value>
      <webElementGuid>d55ca9e9-02e6-40bd-a533-7e98d278afa5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;MuiBox-root css-1n7yudi&quot;]/div[@class=&quot;MuiContainer-root MuiContainer-maxWidthLg css-vyigel&quot;]/div[@class=&quot;css-dxe609&quot;]/div[@class=&quot;css-19jlfq&quot;]/div[@class=&quot;css-1d8k8ed&quot;]/div[@class=&quot;css-1d8k8ed&quot;]/div[@class=&quot;MuiBox-root css-0&quot;]/div[@class=&quot;css-15cnujc&quot;]/div[@class=&quot;css-11nhchy&quot;]/a[1]/button[@class=&quot;MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedAccentGreen MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-disableElevation MuiButton-fullWidth MuiButton-root MuiButton-contained MuiButton-containedAccentGreen MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButton-disableElevation MuiButton-fullWidth css-as8hpy&quot;]</value>
      <webElementGuid>531dff9f-8bfb-4466-acf5-441d71373aad</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[7]</value>
      <webElementGuid>89a77073-a6be-4fdf-b88d-3933a9f231e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[2]/div/div/div/div/div/div[4]/div/div/a/button</value>
      <webElementGuid>88555754-f073-4aa7-93ae-03693cc226c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Uh oh! This page got lost in the web.'])[1]/following::button[1]</value>
      <webElementGuid>23f4852f-c24c-43f2-bf5b-16a2cc1e30f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ERROR 404'])[1]/following::button[1]</value>
      <webElementGuid>d5a02458-710b-4382-8b22-d0c48e9286eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Related resources'])[1]/preceding::button[1]</value>
      <webElementGuid>fa8c80b6-f863-4399-b3d6-2a9590e30ac0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[2]/preceding::button[1]</value>
      <webElementGuid>6292e3dc-cc08-4ea8-8074-bb140b0180b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Return to home']/parent::*</value>
      <webElementGuid>38c37afe-6dc5-479c-ae9a-ca3bfa9a0e2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/a/button</value>
      <webElementGuid>75e9b54d-1e56-43e7-b9aa-7c88a76668ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Return to home' or . = 'Return to home')]</value>
      <webElementGuid>fb5fd56c-1e5b-450e-b2c1-48b115e851e7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
