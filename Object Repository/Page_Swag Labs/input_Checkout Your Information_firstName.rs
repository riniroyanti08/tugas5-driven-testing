<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Checkout Your Information_firstName</name>
   <tag></tag>
   <elementGuidId>351de25c-57bf-492b-be33-e8f1237b401d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='first-name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#first-name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e8c4e16e-cfe0-4006-a58e-d50b025f2ab2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input_error form_input</value>
      <webElementGuid>34e985d0-158c-41e7-8771-e4e0192fd1cc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>First Name</value>
      <webElementGuid>d7b3657b-172b-4c99-a986-98e52d25733b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>c0cb1b51-c5fc-45b5-bcd1-7eef2e49c7e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>firstName</value>
      <webElementGuid>525484fc-c181-4319-ae71-99ca8ba31bce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>first-name</value>
      <webElementGuid>046acffa-062c-425c-9f26-4ce1afd13227</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>firstName</value>
      <webElementGuid>d10cf3dc-2da2-4a19-9641-b42dce4532e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>ff5172ee-7e4f-41fb-b746-5d9bc186a79b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>42f4b777-4576-49f8-8c07-e08330424751</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;first-name&quot;)</value>
      <webElementGuid>923dfab4-355a-407d-b273-0b9eb8cde190</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='first-name']</value>
      <webElementGuid>c264f8f1-df9e-4ef4-bd65-8c4be582a152</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='checkout_info_container']/div/form/div/div/input</value>
      <webElementGuid>80f921c0-41e3-4da2-ae47-1fb88eba1a88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>5b7447e6-44a1-47eb-9139-c0d0e140812d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'First Name' and @type = 'text' and @id = 'first-name' and @name = 'firstName']</value>
      <webElementGuid>5aa0b2e1-91b1-4cc3-9afa-f1b672e656fc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
